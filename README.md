<!--
    TIF RM 18 CNS
    Ade Isman Aji               18 111 179
    Moch Dzulvie Aldyansyah     18 111 211
    Sindu Prakasa Lesmana       18 111 297
    Zeni Malik Abdullah         18 111 237
-->
[![Contributors][contributors-shield]][contributors-url]
[![Matkul][matkul-shield]][matkul-url]
[![Flutter][flutter-shield]][flutter-url]
[![Icon][icon-shield]][icon-url]

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/isman17/piblo/-/blob/master/assets/sttb.jpg">
    <img src="/assets/sttb.jpg" alt="Logo" width="180">
  </a>

  <h3 align="center">Aplikasi Pemilihan Duta Jomblo</h3>

  <p align="center">
    Di situasi pandemi COVID-19, akan lebih aman dan mudah dalam pemilihan duta jomblo dengan aplikasi Pemilihan Duta Jomblo.
    <br />
    Jomblo sehat, Jomblo aman !
    <br />
    Tetap jaga jarak :smiley:
    <br />
    <br />
    <a href="https://gitlab.com/isman17/piblo/-/blob/master/demo%231.mp4">View Demo #1</a>
    <a href="https://gitlab.com/isman17/piblo/-/blob/master/demo%232.mp4">View Demo #2</a>
    ·
    <a href="https://gitlab.com/isman17/piblo/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/isman17/piblo/issues">Request Feature</a>
  </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#built-with">Built With</a></li>
    <li><a href="#contributors">Contributors</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->
## About The Project

Project ini dibuat sebagai tugas UTS Mata Kuliah Mobile Programming 2

<img src="/images/SplashScreen.jpg" alt="drawing" width="200"/>
<img src="/images/Login.jpg" alt="drawing" width="200"/>
<img src="/images/Mading.jpg" alt="drawing" width="200"/>
<br />
<img src="/images/BilikSuara.jpg" alt="drawing" width="200"/>
<img src="/images/KonfirmasiSuara.jpg" alt="drawing" width="200"/>
<img src="/images/MadingAfter.jpg" alt="drawing" width="200"/>

## Built With

* [Flutter](https://flutter.dev/docs/development)
* [Material Icons](https://material.io/resources/icons/?style=baseline)
* [Affection](https://analisadaily.com/imagesfile/arsip/2019/02/kasih-sayang-untuk-sesama-691402-1.jpg)
* [Love](https://asset.kompas.com/crops/PlS9G-0a1BM8hJ70a6rFECEC9L0=/100x67:900x600/750x500/data/photo/2018/01/17/3887882803.jpg)

<!-- Contributors -->
## Contributors

TIF RM 18 CNS :
1. [Ade Isman Aji](https://gitlab.com/isman17)
2. [Moch Dzulvie Aldyansyah](https://gitlab.com/mochdzulvie)
3. [Sindu Prakasa Lesmana](https://gitlab.com/sinduprakasa04)
4. [Zeni Malik Abdulah](https://gitlab.com/zenimalikabdulah)

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<!-- CONTACT -->
## Contact

Project Link: [Piblo](https://github.com/isman17/piblo)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/static/v1?label=Contributors&message=4&color=green
[contributors-url]: https://gitlab.com/isman17/piblo/-/graphs/master
[matkul-shield]: https://img.shields.io/static/v1?label=Mata%20Kuliah&message=Mobile%20Programming%202&color=white
[matkul-url]: https://gitlab.com/isman17/piblo
[flutter-shield]: https://img.shields.io/static/v1?label=Build&message=Flutter&color=blue
[flutter-url]: https://gitlab.com/isman17/piblo
[icon-shield]: https://img.shields.io/static/v1?label=Icons&message=Material%20Icon&color=grey
[icon-url]: https://gitlab.com/isman17/piblo
