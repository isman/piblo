import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:piblo/loginpage.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 5,
        navigateAfterSeconds: new LoginPage(),
        title: new Text(
          'Tugas UTS Mobile Programming\n\nAde Isman Aji\t18 111 179\nMoch Dzulvie Aldyansyah\t18 111 211\nSindu Prakasa Lesmana\t18 111 297\nZeni Malik Abdullah\t18 111 237\n\nTIF RM 18 CNS\nSTTB',
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("Tugas UTS"),
        loaderColor: Colors.red);
  }
}
