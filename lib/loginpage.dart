import 'package:flutter/material.dart';
// import 'package:piblo/biliksuarapage.dart';
import 'package:piblo/madingpage.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController tname = TextEditingController();
  TextEditingController tword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'sttb',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 100.0,
        child: Image.asset('assets/sttb.jpg'),
      ),
    );

    final titleapp = Container(
      child: Center(
        child: Text(
          "Aplikasi Pemilihan Duta Juomblo",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
    );

    final ujicoba = Container(
      child: Text(
        "Username : admin\nPassword : admin",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );

    final titlename = Container(
      child: Text(
        "Enter Your Username Here !",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );

    final uname = TextField(
      controller: tname,
      keyboardType: TextInputType.name,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Your Username',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final titlepass = Container(
      child: Text(
        "Enter Your Password Here !",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );

    final pword = TextField(
      controller: tword,
      keyboardType: TextInputType.visiblePassword,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Your Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginbtn = Padding(
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          color: Colors.green,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.greenAccent.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(2, 4), // changes position of shadow
            ),
          ],
        ),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          splashColor: Colors.blueAccent,
          color: Colors.green,
          child: Text(
            "Login",
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
          ),
          onPressed: () {
            if (tname.text == "admin" && tword.text == "admin") {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MadingPage()));
            } else {
              print(tname.text);
              print(tword.text);
            }
          },
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: ListView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 30.0),
              titleapp,
              SizedBox(height: 30.0),
              ujicoba,
              SizedBox(height: 10.0),
              titlename,
              SizedBox(height: 10.0),
              uname,
              SizedBox(height: 20.0),
              titlepass,
              SizedBox(height: 10.0),
              pword,
              SizedBox(height: 20.0),
              loginbtn
            ],
          ),
        ),
      ),
      // )
    );
  }
}
