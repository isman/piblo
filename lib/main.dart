import 'package:flutter/material.dart';
import 'package:piblo/biliksuarapage.dart';
import 'package:piblo/madingafterpage.dart';
import 'package:piblo/splashscreen.dart';
import 'package:piblo/loginpage.dart';
import 'package:piblo/madingpage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    MadingPage.tag: (context) => MadingPage(),
    BiliksuaraPage.tag: (context) => BiliksuaraPage(),
    MadingAfterPage.tag: (context) => MadingAfterPage(),
  };
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tugas UTS Mobile Programming',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Montserrat Alternates',
      ),
      home: Splash(),
      routes: routes,
    );
  }
}
