import 'package:flutter/material.dart';
import 'package:piblo/madingafterpage.dart';
import 'package:piblo/madingpage.dart';

class BiliksuaraPage extends StatelessWidget {
  var potoi = 'assets/isman.jpg';
  var potoz = 'assets/zeni.jpeg';
  var potos = 'assets/sindu.jpeg';
  var potod = 'assets/dzulvie.jpeg';
  var nomeri = 4;
  var nomerz = 1;
  var nomers = 2;
  var nomerd = 3;

  static String tag = 'biliksuara-page';

  @override
  Widget build(BuildContext context) {
    void showDialog(int nomercalon, String image) {
      showGeneralDialog(
        barrierLabel: "Barrier",
        barrierDismissible: true,
        barrierColor: Colors.black.withOpacity(0.5),
        transitionDuration: Duration(milliseconds: 700),
        context: context,
        pageBuilder: (_, __, ___) {
          return Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 300,
              margin: EdgeInsets.only(bottom: 50, left: 12, right: 12),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(40),
              ),
              child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Yakin Milih Nomer $nomercalon ?\n\nAti - Ati Janji Manis :(\n\n#teamJones\n\n",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        OutlineButton(
                          splashColor: Colors.blueAccent,
                          borderSide: BorderSide(
                              color: Colors.green.shade700, width: 2),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          child: Text(
                            "Ragu ?",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                          onPressed: () => Navigator.pop(context),
                        ),
                        SizedBox(width: 20),
                        Container(
                          height: 50,
                          decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(20),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.greenAccent.withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset:
                                    Offset(2, 4), // changes position of shadow
                              ),
                            ],
                          ),
                          child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              splashColor: Colors.blueAccent,
                              color: Colors.green,
                              child: Text(
                                "Yakin",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                  builder: (_) {
                                    var madingAfter = MadingAfterPage(
                                      nomercalon: nomercalon.toString(),
                                      image: image,
                                    );
                                    return madingAfter;
                                  },
                                ));
                              }),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
        transitionBuilder: (_, anim, __, child) {
          return SlideTransition(
            position:
                Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
            child: child,
          );
        },
      );
    }

    final zeni = Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Image.asset(
              potoz,
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment(0, 5.3),
            // heightFactor: 0,
            child: FloatingActionButton(
              heroTag: nomerz,
              backgroundColor: Colors.white,
              onPressed: () {
                showDialog(nomerz, potoz);
              },
              splashColor: Colors.blueAccent.shade50,
              child: Text(
                "1",
                style: TextStyle(
                    color: Colors.blue.shade700, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );

    final sindu = Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Image.asset(
              potos,
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment(0, 5.3),
            // heightFactor: 0,
            child: FloatingActionButton(
              heroTag: nomers,
              backgroundColor: Colors.white,
              onPressed: () {
                showDialog(nomers, potos);
              },
              splashColor: Colors.blueAccent.shade50,
              child: Text(
                "2",
                style: TextStyle(
                    color: Colors.blue.shade700, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );

    final dzulvie = Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Image.asset(
              potod,
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment(0, 5.3),
            // heightFactor: 0,
            child: FloatingActionButton(
              heroTag: nomerd,
              backgroundColor: Colors.white,
              onPressed: () {
                showDialog(nomerd, potod);
              },
              splashColor: Colors.blueAccent.shade50,
              child: Text(
                "3",
                style: TextStyle(
                    color: Colors.blue.shade700, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );

    final isman = Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 5,
            blurRadius: 7,
            offset: Offset(0, 3), // changes position of shadow
          ),
        ],
      ),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Image.asset(
              potoi,
              height: 170,
              fit: BoxFit.cover,
            ),
          ),
          Align(
            alignment: Alignment(0, 5.3),
            // heightFactor: 0,
            child: FloatingActionButton(
              heroTag: nomeri,
              backgroundColor: Colors.white,
              onPressed: () {
                showDialog(nomeri, potoi);
              },
              splashColor: Colors.blueAccent.shade50,
              child: Text(
                "4",
                style: TextStyle(
                    color: Colors.blue.shade700, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(30.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 20),
            zeni,
            SizedBox(height: 20),
            sindu,
            SizedBox(height: 20),
            dzulvie,
            SizedBox(height: 20),
            isman,
            SizedBox(height: 20),
          ],
        ),
      ),
    );

    return Scaffold(
      body: body,
      bottomNavigationBar: Container(
        height: 55,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(2, 2), // changes position of shadow
            ),
          ],
        ),
        child: Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Ga Jadi Sekarang ?",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
              child: OutlineButton(
                splashColor: Colors.blueAccent.shade50,
                borderSide: BorderSide(color: Colors.green.shade700, width: 2),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MadingPage()));
                },
                color: Colors.white,
                textColor: Colors.green.shade700,
                child: Text(
                  "Mading",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ],
        )),
      ),
    );
  }
}
